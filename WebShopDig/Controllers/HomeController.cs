﻿
using Domain.DataModels;
using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebBusinessLogic;
using WebBusinessLogic.Interfaces;
using WebShopDig.Models;

namespace WebShopDig.Controllers
{
    public class HomeController : Controller
    {

        // GET: Home
        public IRegister _register;
        public ActionResult Index()
        {
            return View();
        }


        public ActionResult Register()
        {


            return View();
        }

        [HttpGet]
        public JsonResult UsernameVerify(string Username)
        {
            var bl = new BusinessLogic();
            _register = bl.GetRegister();

            var status = _register.UsernameResponse(Username);


            return Json(status, JsonRequestBehavior.AllowGet);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Register(URegister data)
        {
            if (ModelState.IsValid) {

                var bl = new BusinessLogic();
                _register = bl.GetRegister();

                var regData = new RegisterData();
                regData.Username = data.Username;
                regData.Password = data.Password;
                regData.Rpassword = data.Rpassword;
                regData.Email = data.Email;
                regData.IP = Request.UserHostAddress;
                regData.uRole =  new URole{ ID = 3, RoleName = "User" };
                regData.Contact = data.Contact;
                regData.RegisterDateTime = DateTime.Now;


                var status = _register.RegistrationAction(regData);

                if (status.Validation == false)
                {
                    ModelState.AddModelError("", status.Message);
                    
                    return View();
                }
                else
                {

                    return RedirectToAction("Index");
                }
            }
            return View();
        }


    }
}