﻿using Domain.DataModels;
using Domain.Enums;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebBusinessLogic;
using WebBusinessLogic.ContextDB;
using WebBusinessLogic.Interfaces;
using WebShopDig.Models;

namespace WebShopDig.Controllers
{
    public class AdminServiceController : Controller
    {
        // GET: AdminService

        public IAdministration _administration;
        [HttpGet]
        public ActionResult CreateProduct()
        {
            UserContext db = new UserContext();
            ULaptop mark1 = new ULaptop();


            mark1.Mark = db.Marks.ToList();// Eroareeeeeeee

            return View(mark1);
        }





        [Authorize(Roles = "Administrator")]

        [HttpPost]
        public ActionResult CreateProduct(ULaptop laptop, HttpPostedFileBase file)
        {
            var bl = new BusinessLogic();

            _administration = bl.GetAdministration();
            
            var entrydata = new LaptopModel();

            entrydata.productID = laptop.ProductID;
            entrydata.mark = laptop.Mark;
            entrydata.nameModel = laptop.NameModel;
            entrydata.processor = laptop.Processor;
            entrydata.frequency_processor = laptop.Frequency_processor;
            entrydata.dateTime = DateTime.Now;
            entrydata.modelRam = laptop.ModelRam;
            entrydata.capacityRam = laptop.CapacityRam;
            entrydata.cantitate = laptop.Cantitate;
            entrydata.added_by = User.Identity.Name;
            entrydata.image = laptop.imagePath;
            if (file !=null)
            {

                file.SaveAs(HttpContext.Server.MapPath("~/Images/") + file.FileName);
            }

            //var status = _administration.AdminAddLaptop(entrydata);
                return View();
        }

        public ActionResult Index()
        {
            return View();
        }
    }
}