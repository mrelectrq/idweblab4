﻿
using Domain.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using WebBusinessLogic;
using WebBusinessLogic.Interfaces;
using WebShopDig.App_Start;
using WebShopDig.Models;

namespace WebShopDig.Controllers
{
    public class LoginController : BaseController
    {

        private readonly IAuthentification _session;
        public LoginController()
        {
            var bl = new BusinessLogic();
            _session = bl.GetLogin();
        }

        // GET: Login
       public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(ULogin data)
        {
            var logindata = new LoginData();
            logindata.Username = data.Username;
            logindata.Password = data.Password;
            logindata.LoginDateTime = DateTime.Now;
            logindata.LoginIp = Request.UserHostAddress;

            var status = _session.UserAuthAction(logindata);
            if (status.Status == true)
            {
               
                var cookies = _session.GenerationCookie(data.Username + ":"+ data.Password);
                ControllerContext.HttpContext.Response.Cookies.Add(cookies);
                FormsAuthentication.SetAuthCookie(data.Username, true);


                //bool rool2 = Roles.Provider.IsUserInRole(data.Username, "Administrator");



                return RedirectToAction("Index", "Home");
            }
            else
            {

                ModelState.AddModelError("", status.Message);
                return View();
            }
                                  
        }

        public ActionResult LogOut ()
        {

            FormsAuthentication.SignOut();
            //ControllerContext.HttpContext.Response.Cookies.Clear();
            return RedirectToAction("Index", "Home");
        }
    }
}