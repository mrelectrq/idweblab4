﻿using Domain.Enums;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Drawing.Imaging;
using System.Linq;
using System.Web;

namespace WebShopDig.Models
{
    public class ULaptop
    {

        public int ProductID { get; set; }

        public string NameModel { get; set; }

        public List<LaptopMark> Mark { get; set; }

        public string Processor { get; set; }

        public string Frequency_processor { get; set; }

        public string ModelRam { get; set; }
        public string CapacityRam { get; set; }

        public int Greutatea { get; set; }

        public int Cantitate { get; set; }

        public string imagePath { get; set; }


    }

   
}