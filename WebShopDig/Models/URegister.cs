﻿using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Build.Tasks;

namespace WebShopDig.Models
{
    public class URegister
    {

        [Key]
        [Required(ErrorMessage ="Necesar de completat acest spatiu")]
        [StringLength(12, MinimumLength =5, ErrorMessage ="Lungimea caracterului trebuie sa ")]
        [System.Web.Mvc.Remote("UsernameVerify","Home",HttpMethod ="GET",ErrorMessage ="Username arleady Exists")]
        public string Username { get; set; }

        [Required(ErrorMessage = "Necesar de completat acest spatiu")]
        [StringLength(25, MinimumLength = 8, ErrorMessage = "Lungimea caracterului trebuie sa ")]
        public string Password { get; set; }
        
        [System.Web.Mvc.Compare("Password",ErrorMessage ="Parola nu corespunde")]
        public string Rpassword { get; set; }
        [Required(ErrorMessage = "Necesar de completat acest spatiu")]
        [RegularExpression("^[a-z0-9_\\+-]+(\\.[a-z0-9_\\+-]+)*@[a-z0-9-]+(\\.[a-z0-9]+)*\\.([a-z]{2,4})$", ErrorMessage = "Format email nu este valid")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Necesar de completat acest spatiu")]
        public string Contact { get; set; }



        public URole uRole { get; set; }

        public DateTime RegisterDateTime { get; set; }

        public string IP { get; set; }
    }
}