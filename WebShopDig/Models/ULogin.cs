﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebShopDig.Models
{
    public class ULogin
    {
        [Required(ErrorMessage ="Necesar de completat acest spatiu")]
        public string Username { get; set; }
        [Required(ErrorMessage = "Necesar de completat acest spatiu")]
        public string Password { get; set; }

        public bool Administrator { get; set; }
    }
}