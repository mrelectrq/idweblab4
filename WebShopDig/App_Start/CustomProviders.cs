﻿using Domain.Enums;
using Domain.Tables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using WebBusinessLogic.ContextDB;

namespace WebShopDig.App_Start
{
    public class CustomProviders : RoleProvider
    {
        public override string ApplicationName { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override void CreateRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new NotImplementedException();
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new NotImplementedException();
        }

        public override string[] GetAllRoles()
        {
            throw new NotImplementedException();
        }

        public override string[] GetRolesForUser(string username)
        {
            string[] roles = new string[] { };

            using (var db = new UserContext())
            {
                UserTable user = db.UserData.FirstOrDefault(u => u.Username == username);
                if (user !=null)
                {
                    URole uRole = db.URoles.Find(user.RoleID);
                    if (uRole != null)
                        roles = new string[] { uRole.RoleName };
                }
            }
            return roles;
        }

        public override string[] GetUsersInRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool IsUserInRole(string username, string roleName)
        {

            bool outputResult = true;

            using (var db = new UserContext())
            {
                UserTable user = db.UserData.FirstOrDefault(u => u.Username == username);
                if (user != null)
                {
                    URole uRole = db.URoles.Find(user.RoleID);
                    if (uRole != null && uRole.RoleName== roleName)
                        outputResult = true;
                }
            }

            return outputResult;

        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override bool RoleExists(string roleName)
        {
            throw new NotImplementedException();
        }
    }
}