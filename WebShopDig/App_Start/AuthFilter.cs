﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Filters;
using WebBusinessLogic;
using WebBusinessLogic.Interfaces;

namespace WebShopDig.App_Start
{
    public class AuthFilter : FilterAttribute, IAuthenticationFilter
    {
        public readonly IAuthentification _session;
        public AuthFilter()
        {
            var bl = new BusinessLogic();
            _session = bl.GetLogin();
        }


        public void OnAuthentication(AuthenticationContext filterContext)
        {
            var apiCookie = HttpContext.Current.Request.Cookies["X-KEY"];
            if (apiCookie != null)
            {
                _session.GetUserByCookie(apiCookie.Value);
                if (apiCookie == null)
                {
                    filterContext.Result = new HttpUnauthorizedResult();
                }

            }
            else filterContext.Result = new HttpUnauthorizedResult();

        }

        public void OnAuthenticationChallenge(AuthenticationChallengeContext filterContext)
        {
            var apiCookie = HttpContext.Current.Request.Cookies["X-KEY"];
            if (apiCookie != null)
            {
                _session.GetUserByCookie(apiCookie.Value);
                if (apiCookie == null)
                {
                    filterContext.Result = new RedirectToRouteResult(
                        new System.Web.Routing.RouteValueDictionary {
                    { "controller", "Login" }, { "action", "Index" }
                       });
                }
            }
            else
                filterContext.Result = new RedirectToRouteResult(
    new System.Web.Routing.RouteValueDictionary {
                    { "controller", "Login" }, { "action", "Index" }
   });

        }
    }
}