﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Enums
{
    public class TypeofDevice
    {
        public int ID { get; set; }
        public string nameDevice { get; set; }
    }
}
