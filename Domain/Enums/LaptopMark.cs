﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Enums
{
    public class LaptopMark
    {
        public int ID { get; set; }

        public string MarkName { get; set; }
    }
}
