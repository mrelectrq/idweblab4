﻿using Domain.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Domain.Tables
{
    public class UserTable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [Display(Name = "Username")]


        public string Username { get; set; }

        [Required]
        [Display(Name = "Password")]


        public string Password { get; set; }

        [Required]
        [Display(Name = "EmailAddress")]

        public string Email { get; set; }

        [Required]
        [Display(Name = "Contact")]
        public string Contact { get; set; }


        public DateTime RegisterDateTime { get; set; }
        public string IP { get; set; }

        [DataType(DataType.Date)]
        public DateTime LastLogin { get; set; }

        public string LastIp { get; set; }

        public int RoleID { get; set; }
    }
}
