﻿using Domain.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Domain.Tables
{
    public class LaptopTable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required]
        [Display(Name = "IDproduct")]
        public int productID { get; set; }
        [Required]
        [Display(Name = "NumeleModelului")]
        public string nameModel { get; set; }
        [Required]
        [Display(Name = "MarcaLaptop")]
        public List<LaptopMark> mark { get; set; }
        [Required]
        [Display(Name = "TipProcesor")]
        public string procesor { get; set; }
        [Required]
        [Display(Name = "FregventaProcesor")]
        public string frequence_procesor { get; set; }
        [Required]
        [Display(Name = "RamModel")]
        public string modelRam { get; set; }
        [Required]
        [Display(Name = "Capacitate")]
        public string capacityRam { get; set; }
        [Required]
        [Display(Name = "Greutatea")]
        public int greutatea { get; set; }
        [Required]
        [Display(Name = "StoculProdusului")]
        public int cantitate { get; set; }
        [Required]
        [Display(Name = "DataInregistrarii")]
        public DateTime dateTime { get; set; }


    }
}
