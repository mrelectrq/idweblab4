﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Response
{
    public class LoginResponse
    {
        public string Message { get; set; }
        public bool Status { get; set; }
        public bool StatusRole { get; set; }
    }
}
