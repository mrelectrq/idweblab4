﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Response
{
    public class CreateResponse
    {
        public string Message { get; set; }
        public bool Status { get; set; }
    }
}
