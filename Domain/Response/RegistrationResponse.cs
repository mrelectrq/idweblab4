﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Response
{
    public class RegistrationResponse
    {
        public bool Validation { get; set; }

        public string Message { get; set; }
    }
}
