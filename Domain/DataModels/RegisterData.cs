﻿using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.DataModels
{
    public class RegisterData
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Rpassword { get; set; }
        public string Email { get; set; }
        public string Contact { get; set; }

        public URole uRole { get; set; }

        public DateTime RegisterDateTime { get; set; }

        public string IP { get; set; }
    }
}
