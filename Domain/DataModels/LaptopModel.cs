﻿using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.DataModels
{
    public class LaptopModel
    {
        public int productID { get; set; }

        public string nameModel { get; set; }

        public List<LaptopMark> mark { get; set; }

        public string processor { get; set; }

        public string frequency_processor { get; set; }

        public string modelRam { get; set; }
        public string capacityRam { get; set; }

        public int greutatea { get; set; }

        public int cantitate { get; set; }

        public object image { get; set; }

        public DateTime dateTime { get; set; }

        public string added_by { get; set; }

    }
}
