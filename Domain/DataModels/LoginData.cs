﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.DataModels
{
    public class LoginData
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string LoginIp { get; set; }
        public DateTime LoginDateTime { get; set; }
    }
}
