﻿using Domain.Tables;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Text;

namespace BusinessLogic.ContextDB
{
    public class SessionContext : DbContext
    {
        public SessionContext ():
            base("name=sessionData")
        {

        }

        public virtual DbSet<SessionUser> SessionUsers { get; set; }
    }
}
