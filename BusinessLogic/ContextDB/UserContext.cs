﻿using Domain.Tables;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Text;

namespace BusinessLogic.ContextDB
{
    public class UserContext : DbContext
    {
        public UserContext() :
            base("name=userData")
        {

        }

        public virtual DbSet<UserTable> UserData { get; set; }
    }
}
