﻿using BusinessLogic.Core;
using BusinessLogic.Interfaces;
using Domain.DataModels;
using Domain.Response;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogic
{
    public class RegisterBL : UserApi, IRegister
    {
        public RegistrationResponse RegistrationAction(RegisterData uRegister)
        {
            return UserRegistration(uRegister);
        }

        public bool UsernameResponse(string username)
        {
            return UsernameVerify(username);
        }
    }
}
