﻿using BusinessLogic.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogic
{
    public class BusinessLogic
    {
        public IRegister GetRegister()
        {
            return new RegisterBL();
        }

        public IAuthentification GetLogin()
        {
            return new AuthentificationBL();
        }

    }
}
