﻿using BusinessLogic.ContextDB;
using Domain.DataModels;
using Domain.Response;
using Domain.Tables;
using Helpers;
using System.Web;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace BusinessLogic.Core
{
    public class UserApi
    {
        internal RegistrationResponse UserRegistration(RegisterData data)
        {

            var resp = new RegistrationResponse();
            resp.Validation = true;
            using (var db = new UserContext())
            {
                var users = db.UserData.FirstOrDefault(m => m.Username == data.Username);
                if (users != null)
                {
                    resp.Validation = false;
                    resp.Message = "This username arleady exists";
                    return resp;

                }
                var email = db.UserData.FirstOrDefault(u => u.Email == data.Email);
                if (email != null)
                {

                    resp.Validation = false;
                    resp.Message = "This Email exists";
                    return resp;

                }

            }

            if (resp.Validation)
            {
                var user = new UserTable();
                user.Username = data.Username;
                user.Password = EncryptString.HashGen(data.Password);
                user.Email = data.Email;
                user.IP = data.IP;
                user.Contact = data.Contact;
                user.uRole = data.uRole;
                user.RegisterDateTime = data.RegisterDateTime;
                using (var db = new UserContext())
                {
                    user.LastLogin = DateTime.Now;
                    user.LastIp = data.IP;
                    db.UserData.Add(user);
                    db.SaveChanges();
                }

                return new RegistrationResponse { Validation = true, Message = "Account added" };
            }
            else return new RegistrationResponse { Message = "Username or Email arleady is used", Validation = false };
        }

        // Verifica daca username exista deja in baza de date:
        public bool UsernameVerify (string username)
        {
            bool validation = true;
            using (var db = new UserContext())
            {
                var users = db.UserData.FirstOrDefault(m => m.Username == username);
                if (users != null)
                {

                    return validation = false;

                }
            }

            return validation;
        }

        /////////////////////////////////////////////////////////////
        public LoginResponse AuthentificationAction(LoginData data)
        {
            var Username = data.Username;
            var Password = EncryptString.HashGen(data.Password);
            UserTable result;
            var validate = new EmailAddressAttribute();
            if (validate.IsValid(Username))
            {

                using (var db = new UserContext())
                {
                    result = db.UserData.FirstOrDefault(u => u.Email == data.Username && u.Password == Password);
                }

                if (result == null)
                {
                    return new LoginResponse { Status = false, Message = "The Username or Password is Incorrect" };
                }
                using (var todo = new UserContext())
                {
                    result.LastIp = data.LoginIp;
                    result.LastLogin = data.LoginDateTime;
                    todo.Entry(result).State = System.Data.Entity.EntityState.Modified;
                    todo.SaveChanges();
                }
                return new LoginResponse { Status = true };
            }
            else
            {
                var pass = EncryptString.HashGen(data.Password);
                using (var db = new UserContext())
                {
                    result = db.UserData.FirstOrDefault(u => u.Username == Username && u.Password == Password);
                }

                if (result == null)
                {
                    return new LoginResponse { Status = false, Message = "The Username or Password is Incorrect" };
                }

                using (var todo = new UserContext())
                {
                    result.LastIp = data.LoginIp;
                    result.LastLogin = data.LoginDateTime;
                    todo.Entry(result).State = System.Data.Entity.EntityState.Modified;
                    todo.SaveChanges();
                }

                return new LoginResponse { Status = true };
            }
        }


        internal HttpCookie CookieGen(string user)
        {
            var Cookie = CookieGenerator.Create(user);
            var apiCookie = new HttpCookie("X-KEY")
            {
                Value = Cookie
            };

            using (var db = new SessionContext())
            {
                SessionUser curent;
                var validate = new EmailAddressAttribute();
                if (validate.IsValid(user))
                {
                    curent = (from e in db.SessionUsers where e.Username == user select e).FirstOrDefault();
                }
                else
                {
                    curent = (from e in db.SessionUsers where e.Username == user select e).FirstOrDefault();
                }


                if (curent != null)
                {
                    curent.CookieString = Cookie;
                    curent.ExpireTime = DateTime.Now.AddMinutes(60);
                    using (var todo = new SessionContext())
                    {
                        todo.Entry(curent).State = System.Data.Entity.EntityState.Modified;
                        todo.SaveChanges();
                    }
                }
                else
                {
                    db.SessionUsers.Add(new SessionUser
                    {
                        Username = user,
                        CookieString = Cookie,
                        ExpireTime = DateTime.Now.AddMinutes(60)
                    });
                    db.SaveChanges();
                }
            }

            return apiCookie;
        }

        internal string FindUserByeCookie(string cookie)
        {
            using (var db = new SessionContext())
            {
                var session = db.SessionUsers.FirstOrDefault(x => x.CookieString == cookie);
                if (session != null)
                {
                    return session.Username;
                }
                else
                {
                    return null;
                }
            }


        }

    }
}
