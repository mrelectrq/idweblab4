﻿using Domain.DataModels;
using Domain.Response;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogic.Interfaces
{
    public interface IRegister
    {
        RegistrationResponse RegistrationAction(RegisterData uRegister);

        bool UsernameResponse(string username);
    }
}
