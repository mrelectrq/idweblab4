﻿using Domain.DataModels;
using Domain.Response;
using Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogic.Interfaces
{
    public interface IAuthentification
    {
        LoginResponse UserAuthAction(LoginData data);
        HttpCookie GenerationCookie(string user);
        string GetUserByCookie(string cookie);
    }
}
