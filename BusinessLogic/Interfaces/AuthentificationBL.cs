﻿using BusinessLogic.Core;
using Domain.DataModels;
using Domain.Response;
using Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogic.Interfaces
{
    public class AuthentificationBL: UserApi, IAuthentification
    {
        public LoginResponse UserAuthAction(LoginData data)
        {
            return AuthentificationAction(data);
        }

        public HttpCookie GenerationCookie(string user)
        {
            return CookieGen(user);
        }

        public string GetUserByCookie(string cookie)
        {
            return FindUserByeCookie(cookie);
        }

    }
}
