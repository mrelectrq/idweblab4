﻿using Domain.Tables;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebBusinessLogic.ContextDB
{
    public class LaptopContext : DbContext
    {
        public LaptopContext() :
            base ("name = LaptopLists")
        {

        }

        public virtual DbSet<LaptopTable> LaptopTables { get; set; }
    }
}
