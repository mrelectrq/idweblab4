﻿using Domain.Enums;
using Domain.Tables;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebBusinessLogic.ContextDB
{
    public class UserContext : DbContext
    {
        public UserContext() :
            base("name=userData")
        {

        }

        public virtual DbSet<UserTable> UserData { get; set; }
        public virtual DbSet<URole> URoles { get; set; }
        public virtual DbSet<LaptopMark> Marks { get; set; }
    }


    public class DataInitializer12 : DropCreateDatabaseAlways<UserContext>
    {
        protected override void Seed(UserContext context)
        {
            context.URoles.Add(new URole { ID = 1, RoleName = "Administrator" });
            context.URoles.Add(new URole { ID = 2, RoleName = "Moderator" });
            context.URoles.Add(new URole { ID = 3, RoleName = "User" });

            context.Marks.Add(new LaptopMark { ID = 1, MarkName = "Asus" });
            context.Marks.Add(new LaptopMark { ID = 2, MarkName = "Acer" });
            context.Marks.Add(new LaptopMark { ID = 3, MarkName = "Lenovo" });
            context.Marks.Add(new LaptopMark { ID = 4, MarkName = "Toshiba" });
            context.Marks.Add(new LaptopMark { ID = 5, MarkName = "Xiaomi" });
            context.Marks.Add(new LaptopMark { ID = 6, MarkName = "Apple" });
            context.Marks.Add(new LaptopMark { ID = 7, MarkName = "Sony" });
            context.Marks.Add(new LaptopMark { ID = 8, MarkName = "HP" });
        }
    }
}
