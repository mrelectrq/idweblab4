﻿using Domain.Tables;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;

namespace WebBusinessLogic.ContextDB
{
    public class SessionContext : DbContext
    {
        public SessionContext() :
            base("name=userData")
        {

        }

        public virtual DbSet<SessionUser> SessionUsers { get; set; }
    }
}
