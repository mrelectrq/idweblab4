﻿using Domain.DataModels;
using Domain.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using WebBusinessLogic.Core;
using WebBusinessLogic.Interfaces;

namespace WebBusinessLogic
{
    public class AuthentificationBL : UserApi, IAuthentification
    {
        public LoginResponse UserAuthAction(LoginData data)
        {
            return AuthentificationAction(data);
        }

        public HttpCookie GenerationCookie(string user)
        {
            return CookieGen(user);
        }

        public string GetUserByCookie(string cookie)
        {
            return FindUserByeCookie(cookie);
        }

    }
}
