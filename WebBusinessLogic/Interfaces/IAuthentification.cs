﻿using Domain.DataModels;
using Domain.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace WebBusinessLogic.Interfaces
{
    public interface IAuthentification
    {
        LoginResponse UserAuthAction(LoginData data);
        HttpCookie GenerationCookie(string user);
        string GetUserByCookie(string cookie);
    }
}
