﻿using Domain.DataModels;
using Domain.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebBusinessLogic.Interfaces
{
    public interface IRegister
    {
        RegistrationResponse RegistrationAction(RegisterData uRegister);

        bool UsernameResponse(string username);
    }
}
