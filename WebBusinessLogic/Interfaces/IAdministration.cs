﻿using Domain.DataModels;
using Domain.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebBusinessLogic.Interfaces
{
    public interface IAdministration
    {
        CreateResponse AdminAddLaptop(LaptopModel model);
    }
}
