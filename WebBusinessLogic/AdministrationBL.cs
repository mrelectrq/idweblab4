﻿using Domain.DataModels;
using Domain.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebBusinessLogic.Core;
using WebBusinessLogic.Interfaces;

namespace WebBusinessLogic
{
    public class AdministrationBL : AdministratorApi, IAdministration
    {
        public CreateResponse AdminAddLaptop(LaptopModel model)
        {
            return AddLaptop(model);
        }
    }
}
