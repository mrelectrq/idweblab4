﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebBusinessLogic.Interfaces;

namespace WebBusinessLogic
{
    public class BusinessLogic
    {
        public IRegister GetRegister()
        {
            return new RegisterBL();
        }

        public IAuthentification GetLogin()
        {
            return new AuthentificationBL();
        }

        public IAdministration GetAdministration()
        {
            return new AdministrationBL();
        }

    }
}

